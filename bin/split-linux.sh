#!/usr/bin/env bash

# Usage:
# ./bin/split.sh [name]
#
# Example:
# ./bin/split.sh support

set -e # 使得脚本里任何一行命令的退出状态码为非零时，shell立即退出
set -x # 使用 “-x” 启用已执行行的调试输出

CURRENT_BRANCH="master"
BASEPATH=$(cd `dirname $0`; cd ../src/; pwd)
REPOS=$@

function split()
{
    # 获取 commit id
    SHA1=`./bin/splitsh-lite-linux --prefix=$1`
    # 强推到远程子仓库
    git push $2 "$SHA1:refs/heads/$CURRENT_BRANCH" -f
}

function remote()
{
    git remote add $1 $2 || true
}

git pull origin $CURRENT_BRANCH

if [[ $# -eq 0 ]]; then
    REPOS=$(ls $BASEPATH)
fi

for REPO in $REPOS ; do
    # 添加子包的远程仓库地址
    remote $REPO git@gitee.com:easy_code/hyperf-$REPO.git

    split "src/$REPO" hyperf-$REPO
done
