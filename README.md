# hyperf 工具类库

### 1. devtool

工程代码生成工具类

使用说明：https://gitee.com/easy_code/hyperf-devtool

```shell
composer require zhen/hyperf-devtool --dev
```
### 2. rocketmq 协程

基于阿里云 rocketmq SDK 封装，实现了协程、连接池、消息可靠投递 等功能

使用说明：https://gitee.com/easy_code/hyperf-rocketmq

```shell
composer require zhen/hyperf-rocketmq
```

### 3. kit

项目公用组件库 **（相关依赖请参考使用说明）**

使用说明：https://gitee.com/easy_code/hyperf-kit

```shell
composer require zhen/hyperf-kit
```

