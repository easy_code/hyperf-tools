<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/05/20 10:17,
 * @LastEditTime: 2022/05/20 10:17
 */
declare(strict_types=1);

namespace Zhen\HyperfRocketMQ\Event;

use Zhen\HyperfRocketMQ\Library\Model\Message as RocketMQMessage;

class ConsumeEvent
{
    /**
     * @var RocketMQMessage
     */
    protected RocketMQMessage $message;

    public function __construct(RocketMQMessage $message)
    {
        $this->message = $message;
    }

    public function getMessage(): RocketMQMessage
    {
        return $this->message;
    }
}
