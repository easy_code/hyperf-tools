<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/5/22 22:44,
 * @LastEditTime: 2022/5/22 22:44
 */
declare(strict_types=1);

namespace Zhen\HyperfRocketMQ\Model;

use Hyperf\DbConnection\Model\Model;

class MqProduceStatusLog extends Model
{
    protected ?string $table = 'rocketmq_produce_status_log';
}
