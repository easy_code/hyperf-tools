<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/5/11 22:41,
 * @LastEditTime: 2022/5/11 22:41
 */
declare(strict_types=1);

namespace Zhen\HyperfRocketMQ\Annotation;

use Attribute;
use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
#[Attribute(Attribute::TARGET_CLASS)]
class Producer extends AbstractAnnotation
{
    /**
     * 驱动.
     */
    public string $poolName = 'default';

    public string $dbConnection = 'default';

    public string $topic = '';

    public string $messageKey = '';

    public string $messageTag = '';

    /**
     * 是否添加 env 后缀（对 topic、groupId、messageTag 起效）
     * 主要为了区分多个环境共用一个实例
     * @var bool
     */
    public bool $addEnvExt = false;
}
