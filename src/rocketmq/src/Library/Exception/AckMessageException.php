<?php

declare(strict_types=1);

namespace Zhen\HyperfRocketMQ\Library\Exception;

use Zhen\HyperfRocketMQ\Library\Constants;
use Zhen\HyperfRocketMQ\Library\Model\AckMessageErrorItem;

/**
 * Ack message could fail for some receipt handles,
 *     and AckMessageException will be thrown.
 * All failed receiptHandles are saved in "$ackMessageErrorItems".
 */
class AckMessageException extends MQException
{
    protected $ackMessageErrorItems;

    public function __construct($code, $message, $previousException = null, $requestId = null, $hostId = null)
    {
        parent::__construct($code, $message, $previousException, Constants::ACK_FAIL, $requestId, $hostId);

        $this->ackMessageErrorItems = [];
    }

    public function addAckMessageErrorItem(AckMessageErrorItem $item)
    {
        $this->ackMessageErrorItems[] = $item;
    }

    public function getAckMessageErrorItems()
    {
        return $this->ackMessageErrorItems;
    }
}
