<?php

declare(strict_types=1);

namespace Zhen\HyperfRocketMQ\Library\Exception;

class MalformedXMLException extends MQException
{
}
