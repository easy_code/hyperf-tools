<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
    'handler' => [
        'http' => [
            Hyperf\HttpServer\Exception\Handler\HttpExceptionHandler::class,
            \Zhen\HyperfKit\Exception\Handler\ValidationExceptionHandler::class,
            \Zhen\HyperfKit\Exception\Handler\TokenExceptionHandler::class,
            \Zhen\HyperfKit\Exception\Handler\NoPermissionExceptionHandler::class,
            \Zhen\HyperfKit\Exception\Handler\NormalStatusExceptionHandler::class,
            \Zhen\HyperfKit\Exception\Handler\AppExceptionHandler::class,
        ],
    ],
];
