<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 10:41,
 * @LastEditTime: 2022/10/07 10:41
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Event;

use \Throwable;

class ExceptionEvent
{
    protected Throwable $throwable;

    public function __construct(Throwable $throwable)
    {
        $this->throwable = $throwable;
    }

    public function getException(): Throwable
    {
        return $this->throwable;
    }
}