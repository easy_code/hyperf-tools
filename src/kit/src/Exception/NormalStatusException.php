<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 12:06,
 * @LastEditTime: 2022/10/07 12:06
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Exception;

/**
 * Class NormalStatusException
 * @package Zhen\HyperfKit\Exception
 * @author lwz
 */
class NormalStatusException extends CoreException
{

}