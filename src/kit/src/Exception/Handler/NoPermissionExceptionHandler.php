<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 10:59,
 * @LastEditTime: 2022/10/07 10:59
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Exception\Handler;


use Hyperf\ExceptionHandler\ExceptionHandler;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Zhen\HyperfKit\CoreResponse;
use Zhen\HyperfKit\Exception\NoPermissionException;

class NoPermissionExceptionHandler extends ExceptionHandler
{
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $this->stopPropagation();
        return make(CoreResponse::class)->noPermission($throwable->getMessage());
    }

    public function isValid(Throwable $throwable): bool
    {
        return $throwable instanceof NoPermissionException;
    }
}