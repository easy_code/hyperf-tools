<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 11:34,
 * @LastEditTime: 2022/10/07 11:34
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Exception\Handler;


use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\Validation\ValidationException;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Zhen\HyperfKit\CoreResponse;

class ValidationExceptionHandler extends ExceptionHandler
{

    /**
     * @param Throwable $throwable
     * @param ResponseInterface $response
     * @return mixed
     * @author lwz
     */
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $this->stopPropagation();
        $body = $throwable->validator->errors()->first();
        return make(CoreResponse::class)->validateError($body);
    }

    /**
     * @param Throwable $throwable
     * @return bool
     * @author lwz
     */
    public function isValid(Throwable $throwable): bool
    {
        return $throwable instanceof ValidationException;
    }
}