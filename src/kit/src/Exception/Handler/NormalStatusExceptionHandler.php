<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 12:08,
 * @LastEditTime: 2022/10/07 12:08
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Exception\Handler;

use Hyperf\ExceptionHandler\ExceptionHandler;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Zhen\HyperfKit\CoreResponse;
use Zhen\HyperfKit\Exception\NormalStatusException;

/**
 * Class NormalStatusExceptionHandler
 * @package Zhen\HyperfKit\Exception\Handler
 * @author lwz
 */
class NormalStatusExceptionHandler extends ExceptionHandler
{
    /**
     * @param Throwable $throwable
     * @param ResponseInterface $response
     * @return mixed
     * @author lwz
     */
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $this->stopPropagation();
        return make(CoreResponse::class)->handleResponseJson($throwable->getCode(), [], $throwable->getMessage());
    }

    /**
     * @param Throwable $throwable
     * @return bool
     * @author lwz
     */
    public function isValid(Throwable $throwable): bool
    {
        return $throwable instanceof NormalStatusException;
    }
}