<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/11/23 23:28,
 * @LastEditTime: 2022/11/23 23:28
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Exception;


use Zhen\HyperfKit\Constants\ResponseCode;

class ValidationException extends CoreException
{
    protected $code = ResponseCode::VALIDATE_FAILED;

    protected $message = '数据验证失败';
}