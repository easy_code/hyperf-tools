<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 11:45,
 * @LastEditTime: 2022/10/07 11:45
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Exception;


class TokenException extends CoreException
{
    public bool $responseErrInfo = false;
}