<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/06 17:06,
 * @LastEditTime: 2022/10/06 17:06
 */
declare(strict_types=1);

namespace Zhen\HyperfKit;

use Hyperf\DbConnection\Model\Model;
use Hyperf\ModelCache\Cacheable;
use Zhen\HyperfKit\Traits\ModelMacroTrait;

class CoreModel extends Model
{
    use Cacheable, ModelMacroTrait;

    /**
     * 隐藏的字段列表
     * @var string[]
     */
    protected array $hidden = ['deleted_at'];

    // 默认页码
    public const DEFAULT_PAGE_NO = 1;

    // 默认分页大小
    public const DEFAULT_PAGE_SIZE = 20;

    // 最大分页数
    public const MAX_PAGE_SIZE = 100;

    // 状态
    public const DISABLE = 0; // 关闭
    public const ENABLE = 1; // 启用

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        //注册常用方法
        $this->registerBase();
    }
}