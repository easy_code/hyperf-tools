<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/06 18:03,
 * @LastEditTime: 2022/10/06 18:03
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Contract;

/**
 * Interface StatusCodeInterface
 * @package Zhen\HyperfKit\Interface
 * 使用 Swoole\Http\Status 代替
 */
interface StatusCodeInterface
{
    // Informational 1xx
    const STATUS_CONTINUE = 100;
    const STATUS_SWITCHING_PROTOCOLS = 101;
    const STATUS_PROCESSING = 102;
    const STATUS_EARLY_HINTS = 103;
    // Successful 2xx
    const STATUS_OK = 200; // 请求成功。一般用于GET与POST请求
    const STATUS_CREATED = 201; // 已创建。成功请求并创建了新的资源
    const STATUS_ACCEPTED = 202; // 已接受。已经接受请求，但未处理完成
    const STATUS_NON_AUTHORITATIVE_INFORMATION = 203; // 非授权信息。请求成功。但返回的meta信息不在原始的服务器，而是一个副本
    const STATUS_NO_CONTENT = 204; // 无内容。服务器成功处理，但未返回内容。在未更新网页的情况下，可确保浏览器继续显示当前文档
    const STATUS_RESET_CONTENT = 205; // 重置内容。服务器处理成功，用户终端（例如：浏览器）应重置文档视图。可通过此返回码清除浏览器的表单域
    const STATUS_PARTIAL_CONTENT = 206; // 是对资源某一部分的请求，服务器成功处理了部分GET请求，响应报文中包含由Content-Range指定范围的实体内容。
    const STATUS_MULTI_STATUS = 207;
    const STATUS_ALREADY_REPORTED = 208;
    const STATUS_IM_USED = 226;
    // Redirection 3xx
    const STATUS_MULTIPLE_CHOICES = 300; // 多种选择。请求的资源可包括多个位置，相应可返回一个资源特征与地址的列表用于用户终端（例如：浏览器）选择
    const STATUS_MOVED_PERMANENTLY = 301; // 永久性重定向。
    const STATUS_FOUND = 302; // 临时性重定向。
    const STATUS_SEE_OTHER = 303; // 查看其它地址。与302类似。使用GET请求查看
    const STATUS_NOT_MODIFIED = 304; // 未修改。所请求的资源未修改，服务器返回此状态码时，不会返回任何资源。客户端通常会缓存访问过的资源，通过提供一个头信息指出客户端希望只返回在指定日期之后修改的资源
    const STATUS_USE_PROXY = 305; // 使用代理。所请求的资源必须通过代理访问
    const STATUS_RESERVED = 306; // 已经被废弃的HTTP状态码
    const STATUS_TEMPORARY_REDIRECT = 307; // 临时重定向。与302类似。使用GET请求重定向，会按照浏览器标准，不会从POST变成GET。
    const STATUS_PERMANENT_REDIRECT = 308;
    // Client Errors 4xx
    const STATUS_BAD_REQUEST = 400; // 客户端请求报文中存在语法错误，服务器无法理解。浏览器会像200 OK一样对待该状态吗
    const STATUS_UNAUTHORIZED = 401; // 请求要求用户的身份认证，通过HTTP认证（BASIC认证，DIGEST认证）的认证信息，若之前已进行过一次请求，则表示用户认证失败
    const STATUS_PAYMENT_REQUIRED = 402; // 保留，将来使用
    const STATUS_FORBIDDEN = 403; // 服务器理解请求客户端的请求，但是拒绝执行此请求
    const STATUS_NOT_FOUND = 404; // 服务器无法根据客户端的请求找到资源（网页）。
    const STATUS_METHOD_NOT_ALLOWED = 405; // 客户端请求中的方法被禁止
    const STATUS_NOT_ACCEPTABLE = 406; // 服务器无法根据客户端请求的内容特性完成请求
    const STATUS_PROXY_AUTHENTICATION_REQUIRED = 407; // 请求要求代理的身份认证，与401类似，但请求者应当使用代理进行授权
    const STATUS_REQUEST_TIMEOUT = 408; // 服务器等待客户端发送的请求时间过长，超时
    const STATUS_CONFLICT = 409; // 服务器完成客户端的PUT请求是可能返回此代码，服务器处理请求时发生了冲突
    const STATUS_GONE = 410; // 客户端请求的资源已经不存在。410不同于404，如果资源以前有现在被永久删除了可使用410代码，网站设计人员可通过301代码指定资源的新位置
    const STATUS_LENGTH_REQUIRED = 411; // 服务器无法处理客户端发送的不带Content-Length的请求信息
    const STATUS_PRECONDITION_FAILED = 412; // 客户端请求信息的先决条件错误
    const STATUS_PAYLOAD_TOO_LARGE = 413; // 由于请求的实体过大，服务器无法处理，因此拒绝请求。
    const STATUS_URI_TOO_LONG = 414; // 请求的URI过长（URI通常为网址），服务器无法处理
    const STATUS_UNSUPPORTED_MEDIA_TYPE = 415; // 服务器无法处理请求附带的媒体格式
    const STATUS_RANGE_NOT_SATISFIABLE = 416; // 客户端请求的范围无效
    const STATUS_EXPECTATION_FAILED = 417; // 服务器无法满足Expect的请求头信息
    const STATUS_IM_A_TEAPOT = 418;
    const STATUS_MISDIRECTED_REQUEST = 421;
    const STATUS_UNPROCESSABLE_ENTITY = 422;
    const STATUS_LOCKED = 423;
    const STATUS_FAILED_DEPENDENCY = 424;
    const STATUS_TOO_EARLY = 425;
    const STATUS_UPGRADE_REQUIRED = 426;
    const STATUS_PRECONDITION_REQUIRED = 428;
    const STATUS_TOO_MANY_REQUESTS = 429;
    const STATUS_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;
    const STATUS_UNAVAILABLE_FOR_LEGAL_REASONS = 451;
    // Server Errors 5xx
    const STATUS_INTERNAL_SERVER_ERROR = 500; // 服务器内部错误，无法完成请求，也可能是web应用存在bug或某些临时故障
    const STATUS_NOT_IMPLEMENTED = 501; // 服务器不支持请求的功能，无法完成请求
    const STATUS_BAD_GATEWAY = 502; // 充当网关或代理的服务器，从远端服务器接收到了一个无效的请求
    const STATUS_SERVICE_UNAVAILABLE = 503; // 由于超载或系统维护，服务器暂时的无法处理客户端的请求。延时的长度可包含在服务器的Retry-After头信息中
    const STATUS_GATEWAY_TIMEOUT = 504; // 充当网关或代理的服务器，未及时从远端服务器获取请求
    const STATUS_VERSION_NOT_SUPPORTED = 505; // 服务器不支持请求的HTTP协议的版本，无法完成处理
    const STATUS_VARIANT_ALSO_NEGOTIATES = 506;
    const STATUS_INSUFFICIENT_STORAGE = 507;
    const STATUS_LOOP_DETECTED = 508;
    const STATUS_NOT_EXTENDED = 510;
    const STATUS_NETWORK_AUTHENTICATION_REQUIRED = 511;
}