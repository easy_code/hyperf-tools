<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 17:35,
 * @LastEditTime: 2022/10/07 17:35
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Abstracts;

use Hyperf\Context\Context;
use Zhen\HyperfKit\Traits\ServiceTrait;

abstract class AbstractService
{
    use ServiceTrait;

    /**
     * 把数据设置为类属性
     * @param array $data
     */
    public function setAttributes(array $data)
    {
        Context::set('attributes', $data);
    }

    /**
     * 魔术方法，从类属性里获取数据
     * @param string $name
     * @return mixed|string
     */
    public function __get(string $name)
    {
        return $this->getAttributes()[$name] ?? '';
    }

    /**
     * 获取数据
     * @return array
     */
    public function getAttributes(): array
    {
        return Context::get('attributes', []);
    }
}