<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 15:34,
 * @LastEditTime: 2022/10/07 15:34
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Traits;

use Hyperf\Database\Model\Builder;

trait ModelMacroTrait
{
    /**
     * 注册常用自定义方法
     * @author lwz
     */
    private function registerBase()
    {
        Builder::macro('setQueryParams', function (array $where, ?string $tableAlias = null) {
            if (empty($where)) {
                return $this;
            }

            /**
             * $field 格式：
             *    格式一：正常筛选， 内容
             *    格式二：根据指定类型筛选；类型：内容
             *      支持的类型：like、l_like、r_like、gt（大于）、gte（大于等于）、lt（小于）、lte（小于等于）
             *          like:字段 => like %内容%
             *          l_like:字段 => like %内容
             *          r_like:字段 => liek 内容%
             *          gt:字段 => 内容
             *          其他后面补全
             */
            foreach ($where as $field => $value) {
                if ($value === '' || is_null($value)) { // 空条件不做处理
                    continue;
                }

                $fieldArgs = explode(':', $field);

                // 根据指定类型匹配
                if (count($fieldArgs) > 1) {
                    switch ($fieldArgs[0]) {
                        case 'like':
                            $this->where($fieldArgs[1], 'like', '%' . $value . '%');
                            break;
                        case 'l_like':
                            $this->where($fieldArgs[1], 'like', '%' . $value);
                            break;
                        case 'r_like':
                            $this->where($fieldArgs[1], 'like', $value . '%');
                            break;
                        case 'gt':
                            $this->where($fieldArgs[1], '>', $value);
                            break;
                        case 'gte':
                            $this->where($fieldArgs[1], '>=', $value);
                            break;
                        case 'lt':
                            $this->where($fieldArgs[1], '<', $value);
                            break;
                        case 'lte':
                            $this->where($fieldArgs[1], '<=', $value);
                            break;
                    }
                    continue;
                }

                // 正常查询
                $field = $tableAlias ? $tableAlias . '.' . $field : $field;
                $this->{is_array($value) ? 'whereIn' : 'where'}($field, $value);
            }
            return $this;
        });
    }
}