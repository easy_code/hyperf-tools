<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 13:41,
 * @LastEditTime: 2022/10/07 13:41
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Traits;


use Hyperf\Di\Annotation\Inject;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Zhen\HyperfKit\CoreResponse;

trait ControllerTrait
{

    #[Inject]
    protected ContainerInterface $container;

    #[Inject]
    protected CoreResponse $response;

    /**
     * 成功状态码
     * @param array|object $data
     * @param string|null $message
     * @return ResponseInterface
     * @author lwz
     */
    protected function success(array|object $data = [], ?string $message = null): ResponseInterface
    {
        return $this->response->success($data, $message);
    }

    /**
     * 错误
     * @param string $message 错误信息
     * @param int|null $code 业务代码编号
     * @param array $data 返回的数据
     * @return ResponseInterface
     * @author lwz
     */
    protected function error(string $message = '', ?int $code = null, array $data = []): ResponseInterface
    {
        return $this->response->error($message, $code, $data);
    }

    /**
     * 重定向
     * @param string $toUrl 重定向的url
     * @param int $status http 状态码
     * @param string $schema 协议
     * @return ResponseInterface
     * @author lwz
     */
    protected function redirect(string $toUrl, int $status = 302, string $schema = 'http'): ResponseInterface
    {
        return $this->response->redirect($toUrl, $status, $schema);
    }

    /**
     * 下载文件
     * @param string $filePath
     * @param string $name
     * @return ResponseInterface
     * @author lwz
     */
    protected function download(string $filePath, string $name = ''): ResponseInterface
    {
        return $this->response->download($filePath, $name);
    }
}