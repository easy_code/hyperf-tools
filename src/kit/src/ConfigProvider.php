<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/06 15:51,
 * @LastEditTime: 2022/10/06 15:51
 */
declare(strict_types=1);

namespace Zhen\HyperfKit;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [
                CoreResponse::class => CoreResponse::class,
            ],
            'publish' => [
                [
                    'id' => 'exception_handler',
                    'description' => 'exception handler.',
                    'source' => __DIR__ . '/../publish/exceptions.php',
                    'destination' => BASE_PATH . '/config/autoload/exceptions.php',
                ],
            ],
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                ],
                'ignore_annotations' => [
                    'mixin',
                ],
            ],
        ];
    }
}