<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 13:43,
 * @LastEditTime: 2022/10/07 13:43
 */
declare(strict_types=1);

namespace Zhen\HyperfKit;


use Zhen\HyperfKit\Traits\ControllerTrait;

/**
 * 控制器基类
 * Class CoreController
 * @package Zhen\HyperfKit
 * @author lwz
 */
class CoreController
{
    use ControllerTrait;
}