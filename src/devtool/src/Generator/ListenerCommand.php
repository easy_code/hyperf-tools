<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/06 13:46,
 * @LastEditTime: 2022/10/06 13:46
 */
declare(strict_types=1);

namespace Zhen\HyperfDevtool\Generator;

use Hyperf\Command\Annotation\Command;

#[Command]

class ListenerCommand extends GeneratorCommand
{
    public function __construct()
    {
        parent::__construct('ext-gen:listener');
        $this->setDescription('Create a new listener class');
    }

    protected function getStub(): string
    {
        return __DIR__ . '/stubs/listener.stub';
    }

    protected function getDefaultNamespace(): string
    {
        return 'App\\' . $this->getModuleInput() . '\\Listener';
    }
}