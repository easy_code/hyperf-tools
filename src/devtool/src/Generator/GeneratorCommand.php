<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/06 13:42,
 * @LastEditTime: 2022/10/06 13:42
 */
declare(strict_types=1);

namespace Zhen\HyperfDevtool\Generator;
use Hyperf\Devtool\Generator\GeneratorCommand as BaiscCommand;
use Symfony\Component\Console\Input\InputArgument;

abstract class GeneratorCommand extends BaiscCommand
{
    /**
     * Get the desired class name from the input.
     */
    protected function getModuleInput(): string
    {
        return trim($this->input->getArgument('module'));
    }

    /**
     * Get the console command arguments.
     */
    protected function getArguments(): array
    {
        return [
            ['module', InputArgument::REQUIRED, 'The name of the module'],
            ['name', InputArgument::REQUIRED, 'The name of the class'],
        ];
    }
}