## 安装
```shell
composer require zhen/hyperf-devtool --dev
```

## 工程目录结构

```text
├── app                                 // 应用程序目录
│   └── Bar                             // bar领域目录
│       └──Controller                   // 控制器目录
│           └──Api                      // 前端相关接口
│           └──Admin                    // 后台相关接口
│       └──Config                       // 模块自定义配置目录
│       └──Events                       // 事件目录
│       └──Listener                     // 事件监听者
│       └──Constants                    // 常量
│       └──Database                     // 数据库迁移
│       └──Request                      // 请求验证目录
│       └──Interfaces                   // 接口目录
│       └──Model                        // 模型目录
│       └──Service                      // 核心业务逻辑
│       └──Dao                          // 数据库访问层
│       └──...                          // 其他自定义目录
│   └── README.md                       // 模块说明
│   └── ...                             // 以后增加的其他模块目录
├── config                              // 配置文件目录
├── core                                // 项目核心目录
│   └──Abstracts                        // 存放抽象类目录
│   └──Console                          // 存放自定义命令目录
│   └──Exception                        // 存放异常接管处理目录
│   └──Library                          // 公共类库目录
│   └──Listener                         // 存放事件监听目录
│   └──Traits                           // 存放复用类目录
├── vendor
```

## 相关命令说明

命令用法： php bin/hyperf.php 领域名称 模块名称（与 hyperf 相同）
```yaml
# 初始化相关命令
+ ext-init:service          Initialize service directory and file  + ext-gen:aspect            Create a new aspect class  
# 代码生成相关命令
+ ext-gen:command           Create a new command class             
+ ext-gen:constant          Create a new constant class            
+ ext-gen:controller        Create a new controller class          
+ ext-gen:job               Create a new job class                 
+ ext-gen:listener          Create a new listener class            
+ ext-gen:middleware        Create a new middleware class          
+ ext-gen:process           Create a new process class             
+ ext-gen:request           Create a new form request class        
+ ext-gen:rocketmq-consumer Create a new rocketmq consumer class   
+ ext-gen:rocketmq-producer Create a new rocketmq producer class   
+ ext-gen:model             Create new model classe+              
+ ext-gen:migration         Generate a new migration file
# 数据迁移相关命令
+ ext-migrate               Run the database migrations            
+ ext-db:seed               Seed the database with records         
+ ext-gen:seeder            Create a new seeder class.
```

使用示例

```shell
# 初始化 Bar 领域的 Foo 功能
php bin/hyperf.php ext-init:service Bar Foo
# 在 Bar 领域下创建控制
php bin/hyperf.php ext-gen:controller Bar TestController
# 在 Bar 领域创建表单验证
php bin/hyperf.php ext-gen:request Bar TestRequest
```

